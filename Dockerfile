################# Run with TLH Team Image ############# 
# Get TLH Team Image with already lib-fw
FROM thanhdc/tlhteam:tlhimage1.0.11 as backend

# Working directory
WORKDIR /u01/app/bccs/os_gem/app

# Copy srcCode for build
COPY . /u01/app/bccs/os_gem/app/src

RUN ls -la /u01/app/bccs/os_gem/app

# Build with ant
RUN /u01/app/bccs/os_gem/app/apache-ant-1.10.5/bin/ant -f /u01/app/bccs/os_gem/app/src/build.xml
RUN ls -la /u01/app/bccs/os_gem/app/src/build/se-backend/artifacts/se_backend_jar

# Copy resource folder to docker
COPY resources /u01/app/bccs/os_gem/resources

ENV SPRING_PROFILES_ACTIVE=LOCAL_PROFILE,HA_ACTIVE_ACTIVE,QUART_TEST_PROFILE,GROUP_PC_TEST_PROFILE,INSTANCE_PROFILE_1
ENV SERVER_PORT=8113
ENV SERVER_NAME=local_8113
ENV LOG_IN_MESSAGE=true
ENV LOG_OUT_MESSAGE=true
ENV LOGGING_CONFIG=../resources/log4j2.xml
ENV SHOW_SQL=true
ENV JDBC_TIMEOUT=15
ENV SHOW_PERFORMANCE_ALL=true

RUN env

# Run app from jar file
CMD java -Duser.timezone=Asia/Ho_Chi_Minh -cp "src/build/se-backend/artifacts/se_backend_jar/*:lib/*:lib/brave-zipkin/*:lib/cxf/*:lib/log4j2/*:lib/spring-batch/*:lib/unittest/*:lib-deploy/*:lib-ws/*:lib-os/*:lib-os/ph-ubl/*:lib-os/sftp/*" -Dlog4j.configurationFile=/u01/app/bccs/os_gem/resources/log4j2.xml -javaagent:/u01/app/bccs/os_gem/app/spring-instrument-4.1.9.RELEASE.jar com.viettel.fw.app.StartApplication

