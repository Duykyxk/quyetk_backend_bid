package com.tlh.team.bidding.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "BID_ROLE")
@Setter
@Getter
public class BidRole {

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "BID_ROLE_ID_GENERATOR", sequenceName = "BID_ROLE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_ROLE_ID_GENERATOR")

    @Column(name = "ROLE_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

}
