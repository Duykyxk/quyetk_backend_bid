package com.tlh.team.bidding.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BidBaseModel {

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ssZ")
    private Date createDate;

    @Column(name = "CREATE_USER", length = 150)
    private String createUser;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE")
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ssZ")
    private Date updateDate;

    @Column(name = "UPDATE_USER", length = 150)
    private String updateUser;
}
