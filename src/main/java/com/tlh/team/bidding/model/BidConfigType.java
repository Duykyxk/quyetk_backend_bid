package com.tlh.team.bidding.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "BID_CONFIG_TYPE")
@Setter
@Getter
public class BidConfigType extends BidBaseModel {
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "BID_CONFIG_TYPE_ID_GENERATOR", sequenceName = "BID_CONFIG_TYPE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_CONFIG_TYPE_ID_GENERATOR")
    @Column(name = "BID_CONFIG_TYPE_ID")
    private Long id;

    @Column(name = "CODE", length = 50, unique = true)
    private String code;

    @Column(name = "NAME", length = 200)
    private String name;

}
