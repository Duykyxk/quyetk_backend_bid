package com.tlh.team.bidding.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "BID_USER")
@Setter
@Getter
public class BidUser {

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "BID_USER_ID_GENERATOR", sequenceName = "BID_USER_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_USER_ID_GENERATOR")

    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String passWord;


}
