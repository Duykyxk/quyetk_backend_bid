package com.tlh.team.bidding.model;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class CustomUser extends org.springframework.security.core.userdetails.User {

    private String userName;

    private String passWord;

    private Collection<GrantedAuthority> authorities;

    public CustomUser(User user, Collection<GrantedAuthority> grantedAuthorities) {
        super(user.getUserName(), user.getPassWord(), grantedAuthorities);
        this.userName = user.getUserName();
        this.passWord = user.getPassWord();
        this.authorities = grantedAuthorities;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

}
