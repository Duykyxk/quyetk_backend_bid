package com.tlh.team.bidding.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "BID_CONFIG")
@Setter
@Getter
public class BidConfig extends BidBaseModel {
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "BID_CONFIG_ID_GENERATOR", sequenceName = "BID_CONFIG_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BID_CONFIG_ID_GENERATOR")
    @Column(name = "BID_CONFIG_ID")
    private Long id;

    @Column(name = "CODE", length = 50, unique = true)
    private String code;

    @Column(name = "BID_CONFIG_TYPE_ID")
    private Long configTypeId;

    @Column(name = "VALUE", length = 1000)
    private String value;

    @Column(name = "DESCRIPTION", length = 1000)
    private String description;

    @Column(name = "STATUS")
    private Long status;

}
