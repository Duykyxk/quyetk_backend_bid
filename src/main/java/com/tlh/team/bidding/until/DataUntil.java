package com.tlh.team.bidding.until;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;

public class DataUntil {

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> T defaultIfNull(T object, T defaultValue) {
        return object != null ? object : defaultValue;
    }

    public static boolean isNullOrEmpty(CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for(int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public static boolean isNullObject(Object obj1) {
        if (obj1 == null) {
            return true;
        } else {
            return obj1 instanceof String ? isNullOrEmpty((CharSequence)obj1.toString()) : false;
        }
    }

    public static Long safeToLong(Object obj1, Long defaultValue) {
        Long result = defaultValue;
        if (obj1 != null) {
            if (obj1 instanceof BigDecimal) {
                return ((BigDecimal)obj1).longValue();
            }

            if (obj1 instanceof BigInteger) {
                return ((BigInteger)obj1).longValue();
            }

            try {
                result = Long.parseLong(obj1.toString());
            } catch (Exception var4) {
            }
        }

        return result;
    }

    public static Long safeToLong(Object obj1) {
        return safeToLong(obj1, 0L);
    }

    public static String safeToString(Object obj1, String defaultValue) {
        return obj1 == null ? defaultValue : obj1.toString();
    }
    public static String safeToString(Object obj1) {
        return safeToString(obj1, "");
    }

}
