package com.tlh.team.bidding.config;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Configuration
public class ConfigManager {
    @PersistenceUnit
    private EntityManagerFactory emf;


    protected SessionFactory getSessionFactory() {
        return emf.unwrap(SessionFactory.class);
    }
}
