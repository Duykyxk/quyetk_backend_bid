package com.tlh.team.bidding.config.oauth;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception{
        http
                .authorizeRequests()
                .antMatchers("/user/register").permitAll()
                .antMatchers("/configtype/**").permitAll()
                .antMatchers("/config/**").permitAll()
                .antMatchers("/actuator/**").permitAll()
                .antMatchers( "/admin/**")
                .hasRole("ADMIN")
                .anyRequest()
                .authenticated();
    }
}
