package com.tlh.team.bidding.controller;

import com.tlh.team.bidding.exception.LogicException;
import com.tlh.team.bidding.model.BidConfigType;
import com.tlh.team.bidding.repository.BidConfigTypeRepo;
import com.tlh.team.bidding.until.DataUntil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/configtype")
public class BidConfigTypeController {

    @Autowired
    BidConfigTypeRepo bidConfigTypeRepo;

    @GetMapping("/getAll")
    ResponseEntity<List<BidConfigType>> getAll() {
        //fake
        List<BidConfigType> bidConfigTypes = new ArrayList<>();
        BidConfigType bidConfigType = new BidConfigType();
        bidConfigType.setCode("Fake");
        bidConfigType.setCreateDate(new Date());
        bidConfigTypes.add(bidConfigType);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigTypes);
        //end fake
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigTypeRepo.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<BidConfigType> getOne(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigTypeRepo.findBidConfigTypeById(id));
    }

    @PostMapping("/add")
    ResponseEntity<BidConfigType> add(@RequestBody BidConfigType bidConfigType) {
        validate(bidConfigType);
        if(DataUntil.isNullOrEmpty(bidConfigType.getCode())) {
            throw new LogicException("Bạn chưa nhập mã code");
        }
        if(!DataUntil.isNullObject(bidConfigTypeRepo.findBidConfigTypeByCode(bidConfigType.getCode()))) {
            throw new LogicException("Mã cấu hình đã tồn tại");
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigTypeRepo.save(bidConfigType));
    }

    private void validate(BidConfigType bidConfigType) {

        if(DataUntil.isNullObject(bidConfigType.getCode())
                || bidConfigType.getCode().length() > 50) {
            throw new LogicException("Mã loại cấu hình ít nhất 50 kí tự");
        }
        if(DataUntil.isNullObject(bidConfigType.getName())
                || bidConfigType.getCode().length() > 200) {
            throw new LogicException("Tên loại cấu hính ít nhất 200 kí tự");
        }

    }

    @PostMapping("/addAll")
    ResponseEntity<List<BidConfigType>> addAll(@RequestBody List<BidConfigType> bidConfigTypeList) {
        //validate
        return ResponseEntity.status(HttpStatus.ACCEPTED).body( bidConfigTypeRepo.saveAll(bidConfigTypeList));
    }

    @PostMapping("/update")
    ResponseEntity<BidConfigType> update(@RequestBody BidConfigType bidConfigType) {
        //validate
        validate(bidConfigType);
        BidConfigType bidConfigTypeOld = bidConfigTypeRepo.findBidConfigTypeById(bidConfigType.getId());
        if(DataUntil.isNullObject(bidConfigType)) {
            throw new LogicException("Cấu hình không tồn tại");
        }
        if(DataUntil.isNullOrEmpty(bidConfigType.getCode())) {
            throw new LogicException("Bạn chưa nhập mã cấu hình");
        }
        String oldCode = bidConfigTypeOld.getCode();
        if(!DataUntil.isNullObject(bidConfigTypeRepo.findBidConfigTypeByCode(bidConfigType.getCode()))) {
            if(!oldCode.equals(bidConfigType.getCode()))
            throw new LogicException("Mã code đã tồn tại");
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigTypeRepo.save(bidConfigType));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<String> delete(@PathVariable Long id) {
        BidConfigType bidConfigType = bidConfigTypeRepo.findBidConfigTypeById(id);
        if(DataUntil.isNullObject(bidConfigType)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("false");
        }
        bidConfigTypeRepo.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("successful");
    }

    @PostMapping("/filter")
    ResponseEntity<List<BidConfigType>> filter(@RequestBody BidConfigType bidConfigTypeFilter) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigTypeRepo.filter(bidConfigTypeFilter));
    }

}
