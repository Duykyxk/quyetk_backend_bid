package com.tlh.team.bidding.controller;

import com.tlh.team.bidding.exception.LogicException;
import com.tlh.team.bidding.model.BidConfig;
import com.tlh.team.bidding.repository.BidConfigRepo;
import com.tlh.team.bidding.until.DataUntil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/config")
public class BidConfigController {

    @Autowired
    BidConfigRepo bidConfigRepo;

    @GetMapping("/getAll")
    ResponseEntity<List<BidConfig>> getAll() {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigRepo.findAll());
    }

    @GetMapping("/{id}")
    ResponseEntity<BidConfig> get(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigRepo.findBidConfigById(id));
    }

    @PostMapping("/add")
    @Transactional(rollbackFor = Exception.class)
    ResponseEntity<BidConfig> add(@RequestBody BidConfig bidConfig) throws LogicException {
        //validate
        if(DataUntil.isNullOrEmpty(bidConfig.getCode())) {
            throw new LogicException("Bạn chưa nhập mã code");
        }
        if(DataUntil.isNullObject(bidConfigRepo.findBidConfigByCode(bidConfig.getCode()))) {
            throw new LogicException("Mã code đã tồn tại");
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigRepo.save(bidConfig));
    }

    @PostMapping("/addAll")
    ResponseEntity<List<BidConfig>> addAll(@RequestBody List<BidConfig> bidConfigs) {
        //validate
        return ResponseEntity.status(HttpStatus.ACCEPTED).body( bidConfigRepo.saveAll(bidConfigs));
    }

    @PostMapping("/update")
    @Transactional(rollbackFor = Exception.class)
    ResponseEntity<BidConfig> update(@RequestBody BidConfig bidConfig) {
        //validate
        if(DataUntil.isNullOrEmpty(bidConfig.getCode())) {
            throw new LogicException("Bạn chưa nhập mã code");
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigRepo.save(bidConfig));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<String> delete(@PathVariable Long id) {
        BidConfig BidConfig = bidConfigRepo.findBidConfigById(id);
        if(DataUntil.isNullObject(BidConfig)) {
            throw new LogicException("Bản ghi không tồn tại");
        }
        bidConfigRepo.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("successful");
    }

    @PostMapping("/filter")
    ResponseEntity<List<BidConfig>> filter(@RequestBody BidConfig bidConfigFilterDTO) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidConfigRepo.filter(bidConfigFilterDTO));
    }

}
