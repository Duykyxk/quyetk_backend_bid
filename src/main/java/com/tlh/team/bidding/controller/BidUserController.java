package com.tlh.team.bidding.controller;

import com.tlh.team.bidding.dto.StatusDTO;
import com.tlh.team.bidding.model.BidUser;
import com.tlh.team.bidding.repository.BidUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class BidUserController {

    @Autowired
    BidUserRepo userRepo;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/userInfo/{id}")
    ResponseEntity<BidUser> getUser(@PathVariable Long id) {
        BidUser bidUser = userRepo.findBidUserById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bidUser);
    }

    @PostMapping("/register")
    ResponseEntity<StatusDTO> register(@RequestBody BidUser bidUser) {
        //validate
        userRepo.save(bidUser);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new StatusDTO("successful",202));
    }

    //Test
    @RequestMapping(value = {"/user"}, produces = "application/json")
    public Map<String, Object> user(OAuth2Authentication user) {
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("user", user.getUserAuthentication().getPrincipal());
        userInfo.put( "authorities", AuthorityUtils.authorityListToSet(user.getUserAuthentication().getAuthorities()));
        return userInfo;
    }

}
