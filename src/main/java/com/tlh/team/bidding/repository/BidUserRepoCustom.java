package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidUser;
import com.tlh.team.bidding.model.CustomUser;

import java.util.List;

public interface BidUserRepoCustom {

    //Test
    List<BidUser> filter(String userName, String passWord);

    CustomUser getCustomUser(String userName);

}
