package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidConfig;

import java.util.List;

public interface BidConfigRepoCustom {

    List<BidConfig> filter(BidConfig bidConfigFilterDTO);

}
