package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidConfigType;

import java.util.List;

public interface BidConfigTypeRepoCustom {

    List<BidConfigType> filter(BidConfigType bidConfigTypeFilterDTO);

}
