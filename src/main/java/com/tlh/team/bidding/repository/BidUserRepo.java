package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidUserRepo extends JpaRepository<BidUser, Long>, BidUserRepoCustom {

    BidUser findBidUserById(Long id);

    BidUser findBidUserByUserName(String username);

    Long removeBidUserById(Long id);

    List<BidUser> findAll();
}
