package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidConfig;
import com.tlh.team.bidding.until.DataUntil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class BidConfigRepoImpl implements BidConfigRepoCustom {

    @Autowired
    EntityManager em;

    @Override
    public List<BidConfig> filter(BidConfig bidConfigFilterDTO) {
        StringBuilder builder = new StringBuilder("SELECT * FROM BID_CONFIG");
        builder.append(" WHERE 1 = 1");
        if (!DataUntil.isNullObject(bidConfigFilterDTO)) {
            HashMap<String, String> params = new HashMap<>();
            if (!DataUntil.isNullOrEmpty(bidConfigFilterDTO.getCode())) {
                if (bidConfigFilterDTO.getCode().contains("%")) {
                    bidConfigFilterDTO.getCode().replace("%", "\\%");
                    builder.append(" AND UPPER(CODE) LIKE :code ESCAPE '\\' ");
                } else {
                    builder.append(" AND UPPER(CODE) LIKE :code ");
                    params.put("code", "%" + bidConfigFilterDTO.getCode().toUpperCase() + "%");
                }
            }
            if (!DataUntil.isNullOrEmpty(bidConfigFilterDTO.getValue())) {
                if (bidConfigFilterDTO.getValue().contains("%")) {
                    bidConfigFilterDTO.getValue().replace("%", "\\%");
                    builder.append(" AND UPPER(VALUE) LIKE :value ESCAPE '\\' ");
                } else {
                    builder.append(" AND UPPER(VALUE) LIKE :value ");
                    params.put("value", "%" + bidConfigFilterDTO.getCode().toUpperCase() + "%");
                }
            }
            if (!DataUntil.isNullObject(bidConfigFilterDTO.getConfigTypeId())) {
                builder.append(" AND BID_CONFIG_TYPE_ID = :configTypeId ");
                params.put("configTypeId", bidConfigFilterDTO.getConfigTypeId().toString());
            }
            if (!DataUntil.isNullObject(bidConfigFilterDTO.getStatus())) {
                builder.append(" AND STATUS = :status ");
                params.put("status", bidConfigFilterDTO.getStatus().toString());
            }
            Query query = em.createNativeQuery(builder.toString(), BidConfig.class);
            for (String key : params.keySet()) {
                query.setParameter(key, params.get(key));
            }
            List<BidConfig> result = query.getResultList();
            if (result.size() != 0) {
                return result;
            }
        }
        return Collections.emptyList();
    }
}
