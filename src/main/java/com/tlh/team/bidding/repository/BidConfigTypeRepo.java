package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidConfigType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BidConfigTypeRepo extends JpaRepository<BidConfigType, Long>, BidConfigTypeRepoCustom {

    BidConfigType findBidConfigTypeById(Long id);

    BidConfigType findBidConfigTypeByCode(String code);

    List<BidConfigType> findAll();

}
