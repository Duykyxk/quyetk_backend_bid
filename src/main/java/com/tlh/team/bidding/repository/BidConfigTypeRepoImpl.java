package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidConfigType;
import com.tlh.team.bidding.until.DataUntil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class BidConfigTypeRepoImpl implements BidConfigTypeRepoCustom {

    @Autowired
    EntityManager em;

    @Override
    public List<BidConfigType> filter(BidConfigType bidConfigTypeFilterDTO) {
        StringBuilder builder = new StringBuilder("SELECT * FROM BID_CONFIG_TYPE");
        builder.append(" WHERE 1 = 1");
        if (!DataUntil.isNullObject(bidConfigTypeFilterDTO)) {
            HashMap<String, String> params = new HashMap<>();
            if (!DataUntil.isNullOrEmpty(bidConfigTypeFilterDTO.getName())) {
                if (bidConfigTypeFilterDTO.getName().contains("%")) {
                    bidConfigTypeFilterDTO.getName().replace("%", "\\%");
                    builder.append(" AND UPPER(NAME) LIKE :name ESCAPE '\\' ");
                } else {
                    builder.append(" AND UPPER(NAME) LIKE :name ");
                    params.put("name", "%" + bidConfigTypeFilterDTO.getName().toUpperCase() + "%");
                }
            }
            if (!DataUntil.isNullOrEmpty(bidConfigTypeFilterDTO.getCode())) {
                if (bidConfigTypeFilterDTO.getCode().contains("%")) {
                    bidConfigTypeFilterDTO.getCode().replace("%", "\\%");
                    builder.append(" AND UPPER(CODE) LIKE :code ESCAPE '\\' ");
                } else {
                    builder.append(" AND UPPER(CODE) LIKE :code ");
                    params.put("code", "%" + bidConfigTypeFilterDTO.getCode().toUpperCase() + "%");
                }
            }
            Query query = em.createNativeQuery(builder.toString(), BidConfigType.class);
            for (String key : params.keySet()) {
                query.setParameter(key, params.get(key));
            }
            List<BidConfigType> result = query.getResultList();
            if (result.size() != 0) {
                return result;
            }
        }
        return Collections.emptyList();
    }

}
