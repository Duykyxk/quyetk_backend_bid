package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidConfig;
import com.tlh.team.bidding.model.BidConfigType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidConfigRepo extends JpaRepository<BidConfig,Long>, BidConfigRepoCustom {

    BidConfig findBidConfigById(Long id);

    BidConfig findBidConfigByCode(String code);

}
