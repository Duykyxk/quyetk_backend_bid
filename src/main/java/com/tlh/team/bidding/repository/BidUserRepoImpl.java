package com.tlh.team.bidding.repository;

import com.tlh.team.bidding.model.BidUser;
import com.tlh.team.bidding.model.CustomUser;
import com.tlh.team.bidding.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BidUserRepoImpl implements BidUserRepoCustom {

    @Autowired
    EntityManager em;

    @Autowired
    BidUserRepo userRepo;

    @Override
    public List<BidUser> filter(String userName, String passWord) {
        List<BidUser> list =  em.createNativeQuery("SELET * FROM BID_USER",BidUser.class).getResultList();
        return list;
    }

    @Override
    public CustomUser getCustomUser(String userName) {
        CustomUser customUser = null;
        BidUser bidUser = userRepo.findBidUserByUserName(userName);
        if (bidUser != null) {
            Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
            //TODO: Test
            GrantedAuthority grantedAuthority;
            if(userName.equals("admin")) {
                grantedAuthority  = new SimpleGrantedAuthority("ROLE_ADMIN");
            } else {
                grantedAuthority  = new SimpleGrantedAuthority("ROLE_USER");
            }
            grantedAuthoritiesList.add(grantedAuthority);
            customUser = new CustomUser(new User(null,bidUser.getUserName(), bidUser.getPassWord()),grantedAuthoritiesList);
        }
        return customUser;
    }
}
