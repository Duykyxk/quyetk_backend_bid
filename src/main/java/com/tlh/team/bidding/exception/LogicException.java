package com.tlh.team.bidding.exception;

public class LogicException extends RuntimeException {

    public LogicException(String message) {
        super(message);
    }
}
