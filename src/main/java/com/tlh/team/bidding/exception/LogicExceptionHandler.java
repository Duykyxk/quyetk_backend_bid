package com.tlh.team.bidding.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class LogicExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseException handlerNotFoundException(NotFoundException ex) {
        return new ResponseException(HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(LogicException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseException handlerCustomException(LogicException ex) {
        return new ResponseException(HttpStatus.NO_CONTENT, ex.getMessage());
    }

//    @ExceptionHandler(LogicException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    public String handlerCustomException(LogicException ex) {
//        return ex.getMessage();
//    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}