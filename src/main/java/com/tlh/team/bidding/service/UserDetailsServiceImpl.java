package com.tlh.team.bidding.service;

import com.tlh.team.bidding.model.CustomUser;
import com.tlh.team.bidding.repository.BidUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private BidUserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CustomUser customUser = userRepo.getCustomUser(username);
        if (customUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return customUser;
    }
}
